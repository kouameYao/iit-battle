import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DetailsComponent } from './pages/details/details.component';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { ChambresComponent } from './pages/chambres/chambres.component';
import { HomeComponent } from './pages/home/home.component';
import { OurServicesComponent } from './pages/our-services/our-services.component';
import { ReservationComponent } from './pages/reservation/reservation.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'accueil'},
  {path: 'accueil', component: HomeComponent},
  {path: 'a-propos', component: AboutUsComponent},
  {path: 'contacts', component: ContactUsComponent},
  {path: 'chambres', component: ChambresComponent},
  {path: 'chambres/details', component: DetailsComponent},
  {path: 'chambres/reservation', component: ReservationComponent},
  {path: 'nos-services', component: OurServicesComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
