import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CustomNavbarComponent } from './@shared/custom-navbar/custom-navbar.component';
import { HomeComponent } from './pages/home/home.component';
import { CustomFooterComponent } from './@shared/custom-footer/custom-footer.component';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { OurServicesComponent } from './pages/our-services/our-services.component';
import { DetailsComponent } from './pages/details/details.component';
import { ChambresComponent } from './pages/chambres/chambres.component';
import { ReservationComponent } from './pages/reservation/reservation.component';

@NgModule({
  declarations: [									
    AppComponent,
      CustomNavbarComponent,
      HomeComponent,
      CustomFooterComponent,
      AboutUsComponent,
      ContactUsComponent,
      OurServicesComponent,
      DetailsComponent,
      ChambresComponent,
      ReservationComponent
   ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
